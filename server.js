process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

var http = require('http');
var https = require('https');

var config = require('./config');
var app = require('./app');
var sslConfig = require('./ssl-config');

var server = null;


if (config.express.https){
    var options = {
        key: sslConfig.key,
        cert: sslConfig.cert
    };

    server = https.createServer(options, app).listen(config.express.port, handleListen);
} else {
    server = http.createServer(app).listen(config.express.port, handleListen);
}

function handleListen(){
    console.log('Express server listening on port ' + config.express.port);
}

