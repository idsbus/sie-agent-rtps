'use strict'

var Rtps = require('../components/rtps/rtps');
var Sftp = require('../functions/sftp');

var ReadCsvFile = require('../functions/read_csv_file');

var rtp_files = require('../components/rtp-files/rtp-files');
var sortides = require('../components/sortides/sortides');

var Converter = require('./converter');

var WriteXml = require('../functions/write-xml');
var Logs = require('../components/console/console-controller');

module.exports = {
    initialize : initialize
};

function initialize(){
    Rtps.initialize(function(err){
        if(err) return setTimeout(initialize, 30000);

        start();
    })
}

function start(){
    Rtps.refreshVersions(function(err){
        if(err) return setTimeout(start, 60000);

        var rtps = Rtps.get();
        checkRtps(rtps, 0, function(){
            Logs.log("All checks finished");
            setTimeout(start, 60000);
        });
    });
}

function checkRtps(rtps, index, callback){
    if(index >=rtps.length) return callback();

    checkRtp(rtps[index], function(){
        WriteXml.writeStatus(rtps[index]);
        checkRtps(rtps, ++index, callback);
    })
}

function setRtpResult(rtp, estat, message){
    Logs.log(message);
    rtp.estat = estat;
    rtp.estat_description = message;
}

function checkRtp(rtp, callback) {
    if (timeToCheckRtp(rtp)) {
        checkRemoteNewVersion(rtp, function(err, new_version_exists){

            if(err) {
                setRtpResult(rtp, 0, 'Checking new RTP version: ' + (err ? err.message.substr(0, 100) : 'version not found'))
                return callback();
            }

            if(!new_version_exists){
                setRtpResult(rtp, 1, 'Ok. Current version: ' + rtp.versio);
                rtp.last_check = new Date();
                return callback();
            }

            Logs.log("New version found: Current version: " + rtp.versio + " -> New version:" + rtp.new_version);

            downloadNewFiles(rtp, function(err){

                if(err) {
                    setRtpResult(rtp, 0, 'Error downloading RTP files: ' + err.message.substr(0, 100));
                    return callback();
                }

                Converter.convertRtpToMysql(rtp, function(err, result){
                    WriteXml.writeResult(rtp);
                    if(err) {
                        setRtpResult(rtp, 0, 'Error converting sortides to mysql: ' + err.message.substr(0,100));
                        return callback();
                    }
                    Logs.log("New version converted to local DB. Dumping sortides...");

                    sortides.dumpSortides(rtp, function(err, dump_result){
                        if(err) {
                            setRtpResult(rtp, 0, 'Error Dumping RTP sortides: ' + err.message.substr(0, 100));
                        } else {
                            rtp.versio = rtp.new_version;
                            rtp.last_check = new Date();
                            setRtpResult(rtp, 1, 'New version downloaded and dumped successfully. New version: ' + rtp.versio);
                            Rtps.setNewVersio(rtp)
                        }
                        return callback();
                    });
                });
            });
        });
    } else {
        callback();
    }
}

function timeToCheckRtp(rtp){
    if(!rtp.active) return false;
    if(!rtp.last_check) return true;
    if(rtp.serie_actual !== rtp.serie_demanada) return true;

    return (new Date() - rtp.last_check.getTime() > rtp.refresh_time * 1000);
}

function checkRemoteNewVersion(rtp, callback){
    var files = [ { file : 'versio.rtp', required : true } ];

    Sftp.downloadFiles({ rtp : rtp, files : files }, function(err){
        if(err) return callback(err);

        ReadCsvFile.readFile({ file_path : rtp.local_path + files[0].file }, function(err, data){
            if(err) return callback(false);

            if(data && data.length && data[0].DATA && data[0].DATA != rtp.versio) {
                rtp.new_version = data[0].DATA;
                callback(null, true);
            } else {
                callback(null, false);
            }
        })
    });
}


function downloadNewFiles(rtp, callback){
    Sftp.downloadFiles({ rtp : rtp, files : rtp_files }, function(err){
        callback(err);
    });
}

