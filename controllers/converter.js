'use strict'

var ReadCsvFile = require('../functions/read_csv_file');
var rtpTables = require('../components/rtp-files/rtp-tables');

var dataToMySql = require('../functions/data-to-mysql');
var Logs = require('../components/console/console-controller');

module.exports = {
    convertRtpToMysql : convertRtpToMysql
}

function convertRtpToMysql(rtp, callback){
    //return callback();

    rtp.taules = [];
    convertAllTables(rtp, 0, callback);
}

function convertAllTables(rtp, index, callback){
    if (index >= rtpTables.tables.length) return callback();

    rtp.taules.push({ name : rtpTables.tables[index].name, result : '', entries : 0, err_entries : 0 });

    readRtpTable(rtp, rtpTables.tables[index], function(err, result){
        if(err) {
            rtp.taules[index].result = err.message.substr(0, 100);
            if(rtpTables.tables[index].required) return callback(err);
        }

        if(!err){
            rtp.taules[index].result = 'OK';
            rtp.taules[index].entries = result.rows;
            rtp.err_entries = 0; // Could be difference between entries and affectedRows
        }

        convertAllTables(rtp, ++index, callback);
    })
}

function readRtpTable(rtp, table, callback){
    ReadCsvFile.readFile({ file_path : rtp.local_path + table.name + '.rtp' }, function(err, data){
        if(err) return callback(err);
        try {
            Logs.log("Converting tables: " + table.name);
            var mysql_data = convertFields(table, rtp, data);

            dataToMySql.del(table, rtp.id, function(err, data){
                if(err) return callback(err);

                insertAllBatches(mysql_data, 0, table, callback)
            })
        } catch(err){
            Logs.log("Error converting tables: " + err.message);
            return callback(err)
        }
    });
}

function insertAllBatches(mysql_data, index, table, callback){
    if(index >= mysql_data.values.length) return callback(null, mysql_data);

    dataToMySql.insert(table, { fields : mysql_data.fields, values : mysql_data.values[index] }, function(err){
        if(err) return callback(err);

        Logs.log("Table: " + table.name + " Inserted batch number " + index);
        insertAllBatches(mysql_data, ++index, table, callback);
    });
}

function convertFields(table, rtp, data){

    if(!data && !data.length) return { fields : null, values : [], rows : 0};
    var field_names = "";
    for (var key in data[0]){
        if(table.mappings[key]) {
            field_names += (field_names ? "," : "") + table.mappings[key];
        }
    }
    field_names += ", idProveedor"

    var chunk_size = 15000;
    var values_batch = [];
    var current_batch = [];

    for(var i=0, total=data.length; i<total; i++){
        var values = "";
        var row = data[i];
        for (var key in row){
            if(table.mappings[key]) {
				
                values += (values ? ',' : '') + (row[key] === "" ? "null" : "'" + row[key].replace(/'/g, "''").replace(/’/,"''").replace(/\x92/,"''").replace(/\x96/,"") + "'");
            }
        }
        current_batch.push("(" + values + ", " + rtp.id + ")");

        if( (i+1) % chunk_size === 0){
            values_batch.push( current_batch.join(',') );
            current_batch = [];
        }
    }

    if(current_batch.length){
        values_batch.push( current_batch.join(',') );
    }

    return { fields : field_names, values : values_batch, rows : data.length};
}