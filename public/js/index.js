document.addEventListener('DOMContentLoaded', function(){

    comp.loadComponents(function(){
        Static.initialize($('#static-container'));
        Notifications.initialize($('#notifications-wrapper'));
    });
});