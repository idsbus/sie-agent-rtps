'use strict'

module.exports = {
    initialize : initialize
};

function initialize(app){
    var console = require('./console/console');
    app.use('/', console);
}

