module.exports = {
    name : 'myhorarissie.versio',
    table : '`myhorarissie`.`versio`',
    id : 'idEntrada',
    columns : [
        "data",
        "serie",
        "serieDemanada",
        "idProveedor"
    ],
    required : [],
    logic_delete : false
};