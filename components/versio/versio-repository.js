'use strict';

var Repository = require('../generic-repository');

module.exports = {
    get : get,
    set : set
}

function get(options, callback){
    var model = 'myhorarissie.versio';

    var filter = { where  : options };

    options.filter=JSON.stringify(filter);

    Repository.get(model, options, callback);
}

function set(options, callback){
    var model = 'myhorarissie.versio';

    Repository.update(model, options, callback);
}