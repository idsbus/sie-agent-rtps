var config = require('../../config');
var Responder = require('../../functions/responder');

var debug = config.debug;

module.exports = {
    setDebugMode : setDebugMode,
    log : log
};

function setDebugMode() {
    return function get(req, res) {
        var result = "Missing parameters"
        if(req.query && typeof req.query.enable_debug !== 'undefined'){
            debug = (req.query.enable_debug == 1);
            result = "Debug changed to " + (req.query.enable_debug == 1);
        }

        Responder.send(null, res, result);
    };
}

function log(content){
    if(debug) console.log(formattedNow() + "> " + content);
}

function formattedNow(){
    var now = new Date();
    return dobleZero(now.getHours()) + ':' + dobleZero(now.getMinutes()) + ':' + dobleZero(now.getSeconds()) + ' ' + dobleZero(now.getDate()) + '/' + dobleZero(now.getMonth()+1) + '/' + now.getFullYear();
}

function dobleZero(value){
    return ('00' + value.toString()).substr(-2);
}