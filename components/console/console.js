'use strict'

var express = require('express');
var router = express.Router();

var CustomController = require('./console-controller');

router.get('/api/console/', CustomController.setDebugMode());

module.exports = router;