module.exports = {
    name : 'mysie.rtps',
    table : '`mysie`.`rtps`',
    id : 'idRTP',
    columns : [
        "nom",
        "pathFTP",
        "codiProveedor",
        "composicio",
        "activat",
        "tipus"
    ],
    required : [],
    logic_delete : false
};