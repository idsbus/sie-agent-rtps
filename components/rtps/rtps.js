'use strict'

var Rtps = require('./rtps-repository');
var Versio = require('../versio/versio-repository');
var Rtp = require('./rtp');
var rtps = [];
var Logs = require('../console/console-controller');

module.exports = {
    initialize : initialize,
    get : get,
    refreshVersions : refreshVersions,
    setNewVersio : setNewVersio
};

function initialize(callback){
    loadRtps(callback)
}

function loadRtps(callback){
    Rtps.get({ tipus : 1, activat : 1}, function(err, data){
        if(err) return callback(err);

        rtps = data.map(function(rtp) { return Rtp.create(rtp) });
        callback();
    })
}

function get(){
    return rtps;
}

function refreshVersions(callback){
    refreshVersionsRtps(0, callback);
}

function refreshVersionsRtps(index, callback){
    if(index >= rtps.length) return callback();

    Versio.get({ idProveedor : rtps[index].id }, function(err, versions){
        if(err) return callback(err);

        if(!versions || !versions.length) return callback(new Error("Versions not found"));

        var versio = versions[0];

        rtps[index].versio = versio.data;
        rtps[index].versio_id = versio.idEntrada;
        rtps[index].serie_actual = versio.serieActual;
        rtps[index].serie_demanada = versio.serieDemanada;

        refreshVersionsRtps(++index, callback);
    });
}

function setNewVersio(rtp, callback){
    Versio.set({ id: rtp.versio_id, data : rtp.versio}, function(err){
        Logs.log("Set new versió: " + (err ? "Error, " + err.message: " Successfully") )
    });
}