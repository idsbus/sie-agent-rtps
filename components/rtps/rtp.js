'use stric';
var config = require('../../config');

module.exports = {
    create : create
};


function create(options){

    var resultatSFTP=[];

    var siErrorVersio=false;
    var errorsSFTP=0;

    var ultimEstat=0;
    var strUltimEstat="";

    var rtp = Object.create({});

    rtp.name=options.nom;
    rtp.path=options.pathFTP;
    rtp.id=options.idRTP;
    rtp.local_path = config.sftp.local_path.replace('{{id}}', rtp.id);
    rtp.active = options.activat;
    rtp.composition=options.composicio;
    rtp.last_check = null;
    rtp.resultOKBD=[];
    rtp.resultERBD=[];
    rtp.refresh_time = 10800;
    rtp.serie_actual = 0;
    rtp.serie_demanada = 0;
    rtp.versio = '';
    rtp.nova_versio = '';
    rtp.estat = 0;
    rtp.estat_description = '';

    rtp.tables = [];

    return rtp;
}

