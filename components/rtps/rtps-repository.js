'use strict';

var Repository = require('../generic-repository');

module.exports = {
    get : get
}

function get(options, callback){
    var model = 'mysie.rtps';

    var filter = { where  : options };

    options.filter=JSON.stringify(filter);

    Repository.get(model, options, callback);
}