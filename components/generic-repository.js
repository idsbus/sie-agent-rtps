var dbm = require('../functions/db_manager');

module.exports = {
    get : get,
    create : create,
    update : update,
    del : del,
    sql : sql
};

function get(model, options, callback){
    dbm.get(model, options, callback)
}

function create(model, options, callback){

    dbm.create(model, options, callback)
}

function update(model, options, callback){
    dbm.update(model, options, callback)
}

function del(model, options, callback){
    dbm.del(model, options, callback)
}

function sql(query, options, callback){
    dbm.sql(query, options, callback)
}