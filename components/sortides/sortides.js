'use strict'
const db = require('../../functions/db_connection');
const dataToMySql = require('../../functions/data-to-mysql');
var Logs = require('../console/console-controller');

module.exports = {
    dumpSortides : dumpSortides
}

function dumpSortides(rtp, callback){
    dataToMySql.del({ name : 'sortides'}, rtp.id, function(err, data) {
        if (err) return callback(err);

        getParadesRtp(rtp, function(err, codisRTP){
            if(err || !codisRTP) return callback(err ? err : new Error("Paraes Rtp is empty"));

            if(codisRTP) {
                var str_sql = getQuery(rtp, codisRTP);
                db.query(str_sql, [], function (err, data_sortides){
                    if(err) return callback(err);
                    Logs.log("Sortides dumped, Dumping destinacions")
                    str_sql = getDestinacionsQuery(rtp);
                    db.query(str_sql, [], function (err, data){
                        return callback(err, data_sortides)
                    });
                });
            }
        });
    });
}

function getQuery(rtp, codisRtp){
    return "INSERT INTO myhorarissie.sortides (idLinia, idTrajecte, hora, idRestriccio, idPeriode, idParadaPuntDesti, idParadaPunt, idDiaAtribut, idProveedor, dataAlta, dataEstimacio)" +
        "( SELECT t1.idLinia, t1.idTrajecte, IF(t1.idParadaPuntSortida=t2.idParadaPunt,t1.horaSortida, Sum(ti1.tempsViatge + ti1.tempsParat) + t1.horaSortida) as hora, " +
        " t1.idRestriccio, t1.idPeriode, t1.idParadaPuntArribada as idParadaPuntDesti, t2.idParadaPunt, t1.idDiaAtribut, " + rtp.id + ", NOW(), NOW() " +
        " FROM myhorarissie.itinerari as t2  " +
        " LEFT JOIN myhorarissie.expedicio as t1 ON t1.idLinia=t2.idLinia  AND t1.idTrajecte=t2.idTrajecte  AND t1.idProveedor=t2.idProveedor AND t1.idGrupHorari = t2.idGrupHorari " +
        " LEFT JOIN (SELECT * FROM myhorarissie.temps_itinerari WHERE temps_itinerari.tempsViatge>=0 AND  temps_itinerari.idProveedor=1 ORDER BY temps_itinerari.idSequencia ASC) as ti1" +
        "  ON ti1.idLinia = t1.idLinia AND ti1.idTrajecte = t1.idTrajecte AND ti1.idGrupHorari = t1.idGrupHorari" +
        " INNER JOIN (SELECT * FROM myhorarissie.temps_itinerari WHERE temps_itinerari.tempsViatge>=0 AND  temps_itinerari.idProveedor=1 ORDER BY temps_itinerari.idSequencia ASC) as ti2" +
        "  ON ti2.idLinia = t1.idLinia AND ti2.idTrajecte = t1.idTrajecte AND ti2.idGrupHorari = t1.idGrupHorari AND ti2.idSequencia=t2.idSequencia" +
        " WHERE t2.idParadaPunt in (" + codisRtp + ") AND t2.idProveedor=" + rtp.id + " AND ti1.idSequencia <= t2.idSequencia " +
        "  AND (t1.idParadaPuntArribada <> t2.idParadaPunt OR (t1.idParadaPuntArribada = t2.idParadaPunt AND t1.idParadaPuntSortida = t2.idParadaPunt ))  " +
        " GROUP BY t1.idLinia, t1.idTrajecte, t1.idRestriccio, t1.idPeriode, t1.idGrupHorari, t1.horaSortida, t1.idParadaPuntArribada, t1.idParadaPuntSortida, t1.idDiaAtribut, t2.idSequencia, t2.idParadaPunt" +
        " )";
}

function getDestinacionsQuery(rtp){
    return "INSERT INTO myhorarissie.destinacions (idParadaPunt, textPanell, textPantalla, textAltres, idProveedor, idLinia) " +
            " SELECT DISTINCT s1.idParadaPuntDesti, SUBSTRING(m1.municipi,1,20), SUBSTRING(m1.municipi,1,30), SUBSTRING(m1.municipi,1,40), " + rtp.id + ", s1.idLinia FROM myhorarissie.sortides as s1" +
            " LEFT JOIN myhorarissie.parada_punt as pp1 ON pp1.idParadaPunt = s1.idParadaPuntDesti AND pp1.idProveedor = s1.idProveedor " +
            " LEFT JOIN myhorarissie.municipis as m1 ON SUBSTRING(m1.codi,1,5)=pp1.idMunicipi " +
            " LEFT JOIN myhorarissie.destinacions as d1 ON d1.idParadaPunt = s1.idParadaPuntDesti AND d1.idProveedor = " + rtp.id + " AND d1.idLinia = s1.idLinia " +
            " WHERE d1.idDestinacio is NULL"
    }

function getParadesRtp(rtp, callback){

    var str_sql = "SELECT DISTINCT codiRTP FROM mysie.paradesrtpperiferics WHERE idProveedor=? ORDER BY codiRTP ASC";
    var parameters = [rtp.id];

    db.query(str_sql, parameters, function(err, parades){
        if(err) return callback(err);
        if(!parades) return callback(null, null);

        var codisRTP = parades.map(function(parada){
            return "'" + parada.codiRTP + "'";
        }).join(',');

        callback(null, codisRTP)
    });
}
