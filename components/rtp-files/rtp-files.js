module.exports = [
    { file: "expedicio.rtp", required: true },
    { file: "grup_horari.rtp", required: true },
    { file: "hores_de_pas.rtp", required: false },
    { file: "itinerari.rtp", required: true },
    { file: "linia.rtp", required: true },
    { file: "operador.rtp", required: true },
    { file: "parada_punt.rtp", required: true },
    { file: "periode.rtp", required: true },
    { file: "prohibicio_transport.rtp", required: false },
    { file: "restriccio.rtp", required: false },
    { file: "temps_itinerari.rtp", required: true },
    { file: "trajecte.rtp", required: true },
    { file: "calendari.rtp", required: true },
    { file: "tipus_dia_2_dia_atribut.rtp", required: false }
]