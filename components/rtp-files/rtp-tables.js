'use stric'

function tables() {
    return [
        { name: "expedicio", mappings: expedicio, required: true },
        { name: "grup_horari", mappings: grup_horari, required: true },
        { name: "hores_de_pas", mappings: hores_de_pas, required: false },
        { name: "itinerari", mappings: itinerari, required: true },
        { name: "linia", mappings: linia, required: true },
        { name: "operador", mappings: operador, required: true },
        { name: "parada_punt", mappings: parada_punt, required: true },
        { name: "periode", mappings: periode, required: true },
        { name: "prohibicio_transport", mappings: prohibicio_transport, required: false },
        { name: "restriccio", mappings: restriccio, required: false },
        { name: "temps_itinerari", mappings: temps_itinerari, required: true },
        { name: "trajecte", mappings: trajecte, required: true },
        { name: "calendari", mappings: calendari, required: true },
        { name: "tipus_dia_2_dia_atribut", mappings: tipus_dia_2_dia_atribut, required: false }
    ]
}

const expedicio = {
    LINIA_ID : "idLinia",
    EXPEDICIO_ID : "idExpedicio",
    PERIODE_ID : "idPeriode",
    TRAJECTE_ID : "idTrajecte",
    DIRECCIO_ID : "idDireccio",
    GRUP_HORARI_ID : "idGrupHorari",
    EXPEDICIO_ID_IMPRESSIO : "idExpedicioImpressio",
    SORTIDA_HORA : "horaSortida",
    SORTIDA_PARADA_PUNT_ID : "idParadaPuntSortida",
    ARRIBADA_PARADA_PUNT_ID : "idParadaPuntArribada",
    DIA_ATRIBUT_ID : "idDiaAtribut",
    NOTA_ID : "idNota",
    BICICLETA_SN : "siBicicleta",
    RESTRICCIO_ID : "idRestriccio",
    TIPUS_VEHICLE_ID : "idTipusVehicle"
};

const grup_horari = {
    GRUP_HORARI_ID: "idGrupHorari",
    GRUP_HORARI_CODI: "codiGrupHorari",
    GRUP_HORARI_NOM: "nom"
};

const hores_de_pas = {
    PERIODE_ID :"idPeriode",
    LINIA_ID :"idLinia",
    EXPEDICIO_ID :"idExpedicio",
    SEQUENCIA_ID :"idSequencia",
    HORA_DE_PAS :"horaDePas",
    TEMPS_PARAT :"tempsParat"
};

const itinerari = {
    PARADA_PUNT_ID :"idParadaPunt",
    LINIA_ID :"idLinia",
    TRAJECTE_ID :"idTrajecte",
    DIRECCIO_ID :"idDireccio",
    SEQUENCIA_ID :"idSequencia",
    DISTANCIA :"distancia",
    PARADA_PUNT_TIPUS :"tipusParadaPunt",
    GRUP_HORARI_ID :"idGrupHorari"
};

const linia = {
    LINIA_ID :"idLinia",
    LINIA_NOM_CURT :"nom",
    LINIA_DESC :"descripcio",
    OPERADOR_ID :"idOperador",
    MODE_TRANSPORT :"modeTransport"
};

const operador = {
    OPERADOR_ID :"idOperador",
    /* OPERADOR_NOM_CURT :"null", */
    OPERADOR_NOM_CURT_PUBLIC :"nomCurt",
    OPERADOR_NOM_COMPLET_PUBLIC :"nom"
};

const parada_punt = {
    PARADA_PUNT_ID :"idParadaPunt",
    PARADA_PUNT_DESC :"descripcio",
    PARADA_PUNT_DESC_CURTA :"decripcioCurta",
    COORD_X :"coordenadaX",
    COORD_Y :"coordenadaY",
    PARADA_ID :"idParada",
    MUNICIPI_ID :"idMunicipi"
};

const periode = {
    PERIODE_ID :"idPeriode",
    PERIODE_DESC :"descripcio",
    PERIODE_DINICI :"inici",
    PERIODE_DFI :"final"
};

const prohibicio_transport = {
    LINIA_ID :"idLinia",
    PARADA_PUNT_INICI_ID :"idParadaPuntInici",
    PARADA_PUNT_DESTI_ID :"idParadaPuntFinal"
};

const restriccio = {
    RESTRICCIO_ID :"idRestriccio",
    PERIODE_ID :"idPeriode",
    DESC1 :"descripcio1",
    DESC2 :"descripcio2",
    DESC3 :"descripcio3",
    DESC4 :"descripcio4",
    DESC5 :"descripcio5",
    DIES :"dies",
    DINICI :"inici",
    DFI :"final"
};

const temps_itinerari = {
    LINIA_ID :"idLinia",
    TRAJECTE_ID :"idTrajecte",
    DIRECCIO_ID :"idDireccio",
    SEQUENCIA_ID :"idSequencia",
    GRUP_HORARI_ID :"idGrupHorari",
    TEMPS_VIATGE :"tempsViatge",
    TEMPS_PARAT :"tempsParat"
};

const trajecte = {
    LINIA_ID :"idLinia",
    TRAJECTE_ID :"idTrajecte",
    DIRECCIO_ID :"idDireccio",
    TRAJECTE_DESC :"descripcio"
};

const calendari = {
    PERIODE_ID :"idPeriode",
    DIA :"dia",
    DIA_DESC :"diaDescripcio",
    TIPUS_DIA_ID :"idTipusDia"
};

const tipus_dia_2_dia_atribut = {
    TIPUS_DIA_ID :"idTipusDia",
    DIA_ATRIBUT_ID :"idDiaAtribut"
};

module.exports = {
    tables : tables()
};