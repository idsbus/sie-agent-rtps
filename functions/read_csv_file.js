'use strict'
var csv=require("csvtojson");

module.exports = {
    readFile : readFile
}

function readFile(options, callback){
    if(!options.file_path) return callback(new Error("Missing parameters"));

    csv({ delimiter : [";"], ignoreEmpty : false}).fromFile(options.file_path, { encoding : 'latin1' })
        .then((jsonObj)=>{
            callback(null, jsonObj);
        }).catch(err => {
            console.error(err.message);
            callback(err);
        });

}