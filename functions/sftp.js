'use strict'
var config = require('../config');
var Logs = require('../components/console/console-controller');

var Client = require('ssh2-sftp-client');
var sftp = new Client();

module.exports = {
    downloadFiles : downloadFiles
};

function connect(options, callback) {

    sftp.connect({
        host: config.sftp.host,
        port: config.sftp.port,
        username: config.sftp.user,
        password: config.sftp.password,
        strictVendor: false
    }).then(() => {
       callback();
    }).catch(err => {
        Logs.log(err, 'catch error');
        return callback(err);
    });
}

function downloadFiles(options, callback){
    if(!options || !options.files || !options.files.length) return callback(new Error("Missing files to download"));

    connect(options, function(err, data){
        if(err) return callback(err);

        Logs.log("Connected to sftp server");
        downloadRemoteFiles(options, 0, function(err){
            sftp.end();
            callback(err);
        });
    });
}

function downloadRemoteFiles(options, index, callback){
    if(index >= options.files.length) return callback();

    sftp.fastGet(options.rtp.path + options.files[index].file, options.rtp.local_path + options.files[index].file).then(()=> {
       Logs.log("Downloaded");
        downloadRemoteFiles(options, ++index, callback);
    }).catch(err => {
        console.error(err);
        if(err && options.files[index].required) {
            callback(err);
        } else{
            downloadRemoteFiles(options, ++index, callback);
        }
    });
}