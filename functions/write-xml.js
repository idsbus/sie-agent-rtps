var fs = require('fs');
var config = require('../config');
var Logs = require('../components/console/console-controller');

module.exports = {
    writeStatus : writeStatus,
    writeResult : writeResult
};

function writeStatus(rtp, callback){
    var data = "";
    var filename = config.xml.path_estat.replace('{{id}}', ('000' + rtp.id.toString()).substr(-3));

    data += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n";

    data += "<estat>\r\n";
    data += "\t<hora>" + formattedNow() + "</hora>\r\n";
    data += "\t<resultat>" + rtp.estat + "</resultat>\r\n";
    data += "\t<descripcio>" + rtp.estat_description + "</descripcio>\r\n";
    data += "</estat>"

    var data_json = {};
    data_json.status = {
        time : new Date().getTime(),
        result : rtp.estat ,
        description : rtp.estat_description
    };

    fs.writeFile(filename.replace('.xml','.json'), JSON.stringify(data_json), 'latin1', function(err){
        if(err) return Logs.log("Error writing JSON file: " + err.message);
        Logs.log("JSON file write successfully");
    });

    fs.writeFile(filename, data, 'latin1', function(err){
        if(callback) callback(err);
    });
}

function writeResult(rtp, callback){

    var data = "";
    var filename = config.xml.path_contingut.replace('{{id}}', ('000' + rtp.id.toString()).substr(-3));

    data += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n";

    data += "<actualitzacio>\r\n";
    data += "\t<data>" + formattedNow() + "</data>\r\n";
    data += "\t<versio>" + rtp.versio+ "</versio>\r\n";
    data += "\t<taules>\r\n";

    rtp.taules.forEach(function(table){
        data += "\t\t<taula>\r\n";
        data += "\t\t\t<nom>" + table.name + "</nom>\r\n";
        data += "\t\t\t<resultat>" + table.result + "</resultat>\r\n";
        data += "\t\t\t<entradesOk>" + table.entries + "</entradesOk>\r\n";
        data += "\t\t\t<entradesError>" + table.err_entries + "</entradesError>\r\n";
        data += "\t\t</taula>\r\n";
    })

    data += "\t</taules>";
    data += "</actualitzacio>";

    fs.writeFile(filename, data, 'latin1', function(err){
        if(callback) callback(err)
    });
}

function formattedNow(){
    var now = new Date();
    return dobleZero(now.getHours()) + ':' + dobleZero(now.getMinutes()) + ':' + dobleZero(now.getSeconds()) + ' ' + dobleZero(now.getDate()) + '/' + dobleZero(now.getMonth()+1) + '/' + now.getFullYear();
}

function dobleZero(value){
    return ('00' + value.toString()).substr(-2);
}