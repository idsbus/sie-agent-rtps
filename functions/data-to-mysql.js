'use strict'

const db = require('./db_connection');

module.exports = {
    del : del,
    insert : insert
}

function del(table, id_proveedor, callback){
    if(!table || !table.name || !id_proveedor) return callback(new Error("Missing parameters"));

    const str_query = "DELETE FROM `myhorarissie`.`" + table.name + "` WHERE idProveedor = ?";

    db.query(str_query,[id_proveedor], callback);
}

function insert(table, data, callback){
    if(!table || !table.name) return callback(new Error("Missing parameters"));

    var str_query = "INSERT INTO `myhorarissie`.`" + table.name + "` (" + data.fields + ") VALUES " + data.values;

    db.query(str_query,[], callback);
}

