var db = require('./db_connection');
var models = require('../models/models');
var Logs = require('../components/console/console-controller');

module.exports = {
    get : get,
    create : create,
    update : update,
    del : del,
    sql : sql
};

function sql(query, options, callback){
    db.query(query, options, callback);
}

function get(str_model, options, callback){
    if(!models[str_model] || !models[str_model].name) return callback(new Error("Model not found or corrupted"));
    model = models[str_model];

    /*
    if(options.foreign_model && (!model.relations || !model.relations[options.foreign_model] || !models[model.relations[options.foreign_model].many_table] ) )
        return callback(new Error("Foreign Model not found or corrupted"));

    model = models[model.relations[options.foreign_model].many_table];
    */
    var parameters = [];
    var query = getQuery(model, options, parameters);
    if(query.err) return callback(query.err);

    Logs.log(query.sql);

    db.query(query.sql, parameters, function(err, rows){
        if(err || !options.filter) return callback(err, rows);

        if(query.filter.include && query.relations) rows = parseJSONIncludedFields(rows, query);

        return callback(null, rows);
    });
}

function parseJSONIncludedFields(rows, query){
    rows.forEach(function(row){
        query.relations.forEach(function(relation){
            row[relation.table] = JSON.parse(row[relation.table]);
            if(row[relation.table] && row[relation.table].length === 1 && row[relation.table][0][relation.many_table_id] === null){
                row[relation.table] = []
            }
        });
    });
    return rows;
}

function getQuery(model, options, parameters){

    try {
        var filter = options.filter ? JSON.parse(options.filter) : null;
    } catch (err){
        return { err : new Error("Wrong filter format") };
    }

    var fields = {
        left_join : ''
    };

    var field_selector = model.columns_mask ? model.columns_mask : (filter && filter.fields ? filter.fields : null);

        fields.columns = field_selector && !options.unmasked ?
                            field_selector.map(function(item) {
                                return ((!filter || !filter.fields_exclude_model_name) ? model.table + '.' : '') + item;
                            })
        : [ model.table + ".*" ];

    var relations = [];
    if(filter && filter.include) {
        if(filter.include.constructor !== Array) filter.include = [ filter.include ];
        relations = filter.include.map(function(include){ return model.relations && model.relations[include] ? model.relations[include] : null });
    }

    var query = {
        filter : filter,
        relations : relations,
        model : model
    };

    if (query.relations.length) fields = getFilteredQuery(model, fields, filter, query.relations);

    fields.where = model.logic_delete && !options.show_deleted ? ` WHERE ${model.table}.deleted = 0 ` : ``;
    if(options.id){
        fields.where += (fields.where === `` ? ` WHERE ` : ` AND `) + ` ${model.table}.${model.id} = ? `;
        parameters.push(options.id);
    }

    if (filter && filter.where){
        var wheres = filter.where.and ? filter.where.and : [ filter.where ];
        fields.where += (fields.where === `` ? ` WHERE ` : ` AND `) + joinColumnsToWhere(wheres, parameters);
    }

    fields.where += (fields.where === `` ? ` WHERE ` : ` AND `) + ` ${model.table}.${model.id} IS NOT NULL `;

    if (filter && filter.group_by) fields.group_by = (fields.group_by ? fields.group_by + ', ' : ' GROUP BY ') + filter.group_by;

    fields.order = (filter && filter.order && filter.order.length) ? 'ORDER BY ' + filter.order.join(',') : null;

    query.sql = `SELECT ${fields.columns.join(',')} FROM ${model.table} ${fields.left_join || ''} ${fields.where} ${fields.group_by || ''} ${fields.order || ''};`;

    return query;
}

function joinColumnsToWhere(and_columns, parameters){
    // Revise this function to protect against sql injection
    var wheres = and_columns.map(function(column){
        return Object.getOwnPropertyNames(column).map(function(key){
            if(column[key].gt){
                parameters.push(column[key].gt);
                return `${convertFieldNameOperators(key)} > ?`;
            } else if(column[key].lt){
                parameters.push(column[key].lt);
                return `${convertFieldNameOperators(key)} < ?`;
            } else if(column[key].gte){
                parameters.push(column[key].gte);
                return `${convertFieldNameOperators(key)} >= ?`;
            } else if(column[key].lte){
                parameters.push(column[key].lte);
                return `${convertFieldNameOperators(key)} <= ?`;
            } else {
                parameters.push(column[key]);
                return `${convertFieldNameOperators(key)} = ?`;
            }
        });
    })[0]

    return wheres.join(' AND ');
}

function convertFieldNameOperators(field){
    // We convert operations descriptors by the operator. for instance (start_plus_duration -> start + duration)
    return field.replace(/_plus_/g,'+')
}

function getFilteredQuery(model, fields, filter, relations){

    relations.forEach(function(relation){
        fields = relation.many_table && models[relation.many_table] ? getFilteredQueryByMany(model, fields, filter, relation) : getFilteredQueryByOne(model, fields, filter, relation);
    })

    return fields;
}

function getFilteredQueryByMany(model, fields, filter, relation){

    //TODO: Add possible multiple left joins using an array.

    var foreign_model = models[relation.many_table] ? models[relation.many_table] : null;
    var foreign_columns = foreign_model.columns_mask ? foreign_model.columns_mask : foreign_model.columns;

    var foreign_column_pairs = foreign_columns.map(function(column){
        return `'${column}', \`${foreign_model.name}\`.${column}`;
    }).join(',');

    fields.columns.push(`JSON_ARRAYAGG( JSON_OBJECT( ${foreign_column_pairs} )) as ${filter.include}`);

    fields.left_join += (` LEFT JOIN ( \`${relation.table}\`, \`${relation.many_table}\` ) ON ` +
        ` ( ${relation.table}.${relation.foreign_key} = ${model.table}.${relation.column} ` +
        ` AND ${relation.many_table}.${relation.many_table_id} = ${relation.table}.${relation.foreign_many_key} ` +
        ( foreign_model.logic_delete ? ` AND ${relation.table}.${foreign_model.logic_delete} = 0 )` : `)` ) );

    fields.group_by = `${fields.group_by ? ', ' : ' GROUP BY '} ${model.table}.${relation.column}`;

    return fields;
}

function getFilteredQueryByOne(model, fields, filter, relation){

    var foreign_model = models[relation.table] ? models[relation.table] : null;
    var foreign_columns = foreign_model.columns_mask ? foreign_model.columns_mask : foreign_model.columns;

    var foreign_column_pairs = foreign_columns.map(function(column){
        return `'${column}', \`${foreign_model.name}\`.${column}`;
    }).join(',');

    fields.columns.push(`JSON_OBJECT( ${foreign_column_pairs} ) as ${relation.table}`);

    fields.left_join += (` LEFT JOIN ( \`${relation.table}\` ) ON ` +
        ` ( ${relation.table}.${relation.foreign_key} = ${model.table}.${relation.column} ` +
        ( foreign_model.logic_delete ? ` AND ${relation.table}.${foreign_model.logic_delete} = 0 )` : `)` ) );

    // fields.group_by = ` GROUP BY ${model.name}.${model.id}`; //--> Ojo en el algun moment ho vam afegir per alguna rao

    return fields;
}

function create(str_model, options, callback){

    if(!models[str_model] || !models[str_model].name) return callback(new Error("Model not found or corrupted"));
    var model = models[str_model];

    var required = model.required || [];

    var missing_parameters = required.reduce(function(missing, item){
        return missing || typeof options[item] === 'undefined'
    }, false);

    if(missing_parameters) return callback(new Error("Missing Parameters"));

    var columns = model.columns.filter(function(item) {
        return typeof options[item] !== 'undefined'
    });

    var parameters = columns.filter(function(item){
            return options[item].toString().indexOf('{{') < 0
        }).map(function(item){
            return options[item];
        });

    var str_query = "INSERT INTO " + model.table + " " +
        " (" + columns.join(',') + ") VALUES " +
        " (" + columns.map(function(item){ return getValue(item, options) }).join(',') + " ) ";

    db.query(str_query, parameters, function(err, result){

        if(err) return callback(err);
        if(!result || !result.insertId) return callback(new Error("Nothing created"));

        var str_query = "SELECT * FROM " + model.table + " " +
            " WHERE " + model.id + " = " + result.insertId + " ";

        db.query(str_query, {}, function(err, rows){
            callback(err, (rows && rows.length) ? rows[0] : {});
        });
    });
}

function update(str_model, options, callback){

    if(!models[str_model] || !models[str_model].name || !models[str_model].id || !models[str_model].columns) return callback(new Error("Model not found or corrupted"));
    var model = models[str_model];

    if(typeof options['id'] === 'undefined') return callback(new Error("Missing key_id Parameter"));

    var columns = model.columns.filter(function(item) {
        return typeof options[item] !== 'undefined'
    });

    var parameters = columns.filter(function(item){
        return options[item].toString().indexOf('{{') < 0
    }).map(function(item){
        return options[item];
    });

    var id = options['id'];
    parameters.push(id);

    var str_query = "UPDATE " + model.table + " SET " +
                    " " + columns.map(function(item){ return item + '=' + getValue(item, options) }).join(',') + " " +
                    " WHERE " + model.id + "=?;";

    Logs.log(str_query, parameters );

    db.query(str_query, parameters, function(err, result){

        if(err) return callback(err);

        var str_query = "SELECT * FROM " + model.table + " " +
                        " WHERE " + model.id + "=?; ";

        db.query(str_query, [id], function(err, rows){
            callback(err, (rows && rows.length) ? rows[0] : {});
        });
    });
}

function del(str_model, options, callback){

    if(!models[str_model] || !models[str_model].name) return callback(new Error("Model not found or corrupted"));
    var model = models[str_model];

    if(typeof options['id'] === 'undefined') return callback(new Error("Missing key_id Parameter"));
    var id = options['id'];

    var str_query = model.logic_delete ? `UPDATE ${model.table} SET ${model.logic_delete} = 1 ` : `DELETE FROM ${model.table} `;
    str_query +=  ` WHERE ${model.id}=?; `;

    Logs.log(str_query, [id]);
    db.query(str_query, [id], function(err, rows){
        if(err) return callback(err);

        var data = {};
        if(rows && rows.affectedRows > 0){
            data[models[str_model].id] = options.id;
        }

        callback(null, data);
    });
}

function getValue(item, options){
    return options[item].toString().indexOf('{{') > -1 ? options[item].replace('{{','').replace('}}','') : '?'
}
